//
//  ViewController.m
//  NearestMe
//
//  Created by Apple on 28/09/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import "ViewController1.h"
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"

@interface ViewController1 ()

@end




@implementation ViewController1
@synthesize list;
@synthesize tbl;
@synthesize filteredListContent, savedSearchTerm, savedScopeButtonIndex, searchWasActive;

@synthesize tbl_view;

NSMutableArray *table_list;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    tbl_view.layer.cornerRadius=8.0f;
    tbl_view.layer.masksToBounds=YES;
    tbl_view.layer.borderColor=[[UIColor grayColor]CGColor];
    tbl_view.layer.borderWidth=1.0f;
   
    ListOfItems=[[NSMutableArray alloc] init];
    ListOfItems1=[[NSMutableArray alloc] init];

	//AddBuddyidItems=[[NSMutableArray alloc] init];
	
	searching = NO;
	
	letUserSelectRow = YES;
    
    
   // itemsearch.tintColor = [UIColor colorWithRed:153/255.0 green:102/255.0 blue:51/255.0 alpha:1.0];
    itemsearch.tintColor = [UIColor colorWithRed:0/255.0 green:70/255.0 blue:51/175.0 alpha:1.0];

    
    list=[[NSMutableArray alloc]initWithObjects:@"accounting",
          
          @"airport",
          
          @"amusement_park",
          
          @"aquarium",
          
          @"art_gallery",
          
          @"atm",
          
          @"bakery",
          
          @"bank",
          
          @"bar",
          
          @"beauty_salon",
          
          @"bicycle_store",
          
          @"book_store",
          
          @"bowling_alley",
          
          @"bus_station",
          
          @"cafe",
          
          @"campground",
          
          @"car_dealer",
          
          @"car_rental",
          
          @"car_repair",
          
          @"car_wash",
          
          @"casino",
          
          @"cemetery",
          
          @"church",
          
          @"city_hall",
          
          @"clothing_store",
          
          @"convenience_store",
          
          @"courthouse",
          
          @"dentist",
          
          @"department_store",
          
          @"doctor",
          
          @"electrician",
          
          @"electronics_store",
          
          @"embassy",
          
          @"establishment",
          
          @"finance",
          
          @"fire_station",
          
          @"florist",
          
          @"food",
          
          @"funeral_home",
          
          @"furniture_store",
          
          @"gas_station",
          
          @"general_contractor",
          
          @"grocery_or_supermarket",
          
          @"gym",
          
          @"hair_care",
          
          @"hardware_store",
          
          @"health",
          
          @"hindu_temple",
          
          @"home_goods_store",
          
          @"hospital",
          
          @"insurance_agency",
          
          @"jewelry_store",
          
          @"laundry",
          
          @"lawyer",
          
          @"library",
          
          @"liquor_store",
          
          @"local_government_office",
          
          @"locksmith",
          
          @"lodging",
          
          @"meal_delivery",
          
          @"meal_takeaway",
          
          @"mosque",
          
          @"movie_rental",
          
          @"movie_theater",
          
          @"moving_company",
          
          @"museum",
          
          @"night_club",
          
          @"painter",
          
          @"park",
          
          @"parking",
          
          @"pet_store",
          
          @"pharmacy",
          
          @"physiotherapist",
          
          @"place_of_worship",
          
          @"plumber",
          
          @"police",
          
          @"post_office",
          
          @"real_estate_agency",
          
          @"restaurant",
          
          @"roofing_contractor",
          
          @"rv_park",
          
          @"school",
          
          @"shoe_store",
          
          @"shopping_mall",
          
          @"spa",
          
          @"stadium",
          
          @"storage",
          
          @"store",
          
          @"subway_station",
          
          @"synagogue",
          
          @"taxi_stand",
          
          @"train_station",
          
          @"travel_agency",
          
          @"university",
          
          @"veterinary_care",
          
          @"zoo",nil];
    
    table_list=[[NSMutableArray alloc]initWithObjects:@"Accounting",
                
                @"Airport",
                
                @"Amusement park",
                
                @"Aquarium",
                
                @"Art gallery",
                
                @"Atm",
                
                @"Bakery",
                
                @"Bank",
                
                @"Bar",
                
                @"Beauty salon",
                
                @"Bicycle store",
                
                @"Book store",
                
                @"Bowling alley",
                
                @"Bus station",
                
                @"Cafe",
                
                @"Campground",
                
                @"Car dealer",
                
                @"Car rental",
                
                @"Car repair",
                
                @"Car wash",
                
                @"Casino",
                
                @"Cemetery",
                
                @"Church",
                
                @"City hall",
                
                @"Clothing store",
                
                @"Convenience store",
                
                @"Courthouse",
                
                @"Dentist",
                
                @"Department store",
                
                @"Doctor",
                
                @"Electrician",
                
                @"Electronics store",
                
                @"Embassy",
                
                @"Establishment",
                
                @"Finance",
                
                @"Fire station",
                
                @"Florist",
                
                @"Food",
                
                @"Funeral home",
                
                @"Furniture store",
                
                @"Gas station",
                
                @"General contractor",
                
                @"Grocery or supermarket",
                
                @"Gym",
                
                @"Hair care",
                
                @"Hardware store",
                
                @"Health",
                
                @"Hindu temple",
                
                @"Home goods store",
                
                @"Hospital",
                
                @"Insurance agency",
                
                @"Jewelry store",
                
                @"Laundry",
                
                @"lawyer",
                
                @"Library",
                
                @"Liquor store",
                
                @"Local government office",
                
                @"Locksmith",
                
                @"Lodging",
                
                @"Meal delivery",
                
                @"Meal takeaway",
                
                @"Mosque",
                
                @"Movie rental",
                
                @"Movie theater",
                
                @"Moving company",
                
                @"Museum",
                
                @"Night club",
                
                @"Painter",
                
                @"Park",
                
                @"Parking",
                
                @"Pet store",
                
                @"Pharmacy",
                
                @"Physiotherapist",
                
                @"Place of worship",
                
                @"Plumber",
                
                @"Police",
                
                @"Post office",
                
                @"Real estate agency",
                
                @"Restaurant",
                
                @"Roofing contractor",
                
                @"Rv park",
                
                @"School",
                
                @"Shoe store",
                
                @"Shopping mall",
                
                @"Spa",
                
                @"Stadium",
                
                @"Storage",
                
                @"Store",
                
                @"Subway station",
                
                @"Synagogue",
                
                @"Taxi stand",
                
                @"Train station",
                
                @"Travel agency",
                
                @"University",
                
                @"Veterinary care",
                
                @"Zoo",nil];
    
    
    self.filteredListContent = [NSMutableArray arrayWithCapacity:[self.list count]];

    if (self.savedSearchTerm)
	{
        [self.searchDisplayController setActive:self.searchWasActive];
        [self.searchDisplayController.searchBar setSelectedScopeButtonIndex:self.savedScopeButtonIndex];
        [self.searchDisplayController.searchBar setText:savedSearchTerm];
        
        self.savedSearchTerm = nil;
    }
	
	[self.tbl reloadData];
	self.tbl.scrollEnabled = YES;
    
   
    
    
     NSLog(@"hi1");

    
    
	// Do any additional setup after loading the view, typically from a nib.
}
- (void)viewDidUnload
{
	self.filteredListContent = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    // save the state of the search UI so that it can be restored if the view is re-created
    self.searchWasActive = [self.searchDisplayController isActive];
    self.savedSearchTerm = [self.searchDisplayController.searchBar text];
    self.savedScopeButtonIndex = [self.searchDisplayController.searchBar selectedScopeButtonIndex];
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    if (searching)
    {
       return 1; 
    }
    else{
    return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (searching) {
        return [ListOfItems count];
    }
    else{
        return [list count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
    
    
    cell = [[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"Cell"] ;
    NSString *temp_str;
    
    if (searching)
    {
        temp_str = [ListOfItems objectAtIndex:indexPath.row];
        CGRect frame1 = CGRectMake(10, 20, 250, 15);
        UILabel *title_label = [[UILabel alloc] initWithFrame:frame1];
        title_label.textAlignment = UITextAlignmentLeft;
        title_label.text=[NSString stringWithFormat:@"%@",temp_str] ;
        title_label.lineBreakMode = UILineBreakModeWordWrap;
        title_label.numberOfLines = 2;
        title_label.textColor=[UIColor blackColor];
        title_label.backgroundColor=[UIColor clearColor];
        [ title_label setFont: [UIFont fontWithName:@"Arial" size:14.0f]];
        //title_label.tag = 1;
        [cell.contentView addSubview:title_label];
       
        //cell.textLabel.text=temp_str;
        
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        
    }
    
    else
    {
    
    temp_str = [table_list objectAtIndex:indexPath.row];
    CGRect frame1 = CGRectMake(10, 20, 250, 15);
    UILabel *title_label = [[UILabel alloc] initWithFrame:frame1];
    title_label.textAlignment = UITextAlignmentLeft;
    title_label.text=[NSString stringWithFormat:@"%@",temp_str] ;
    title_label.lineBreakMode = UILineBreakModeWordWrap;
    title_label.numberOfLines = 2;
    title_label.textColor=[UIColor blackColor];
    title_label.backgroundColor=[UIColor clearColor];
    [ title_label setFont: [UIFont fontWithName:@"Arial" size:14.0f]];
    //title_label.tag = 1;
    [cell.contentView addSubview:title_label];
  
    //cell.textLabel.text=temp_str;
    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (searching) {
        return 42 ;
    }
    else{
    return 42 ;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   

            if([[UIScreen mainScreen] bounds].size.height == 480)
            {
                NSLog(@"IPHONE4");
                Display *Placescls=[[Display alloc] initWithNibName:@"Display_iPhone4" bundle:nil];
                Placescls.get_str=[list objectAtIndex:indexPath.row];
                Placescls.get_title=[table_list objectAtIndex:indexPath.row];
                [self presentModalViewController:Placescls animated:NO];
                //iPhone, iPhone retina
            }
         
            else if([[UIScreen mainScreen] bounds].size.height == 568)
                
            {
                NSLog(@"IPHONE5");
                Display *Placescls=[[Display alloc] initWithNibName:@"Display_iPhone" bundle:nil];
                Placescls.get_str=[list objectAtIndex:indexPath.row];
                Placescls.get_title=[table_list objectAtIndex:indexPath.row];
                [self presentModalViewController:Placescls animated:NO];
            }



}

-(IBAction)home_btn:(id)sender
{
 
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }

}


// ************************************ SearBar Function ******************************** //

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar

{
	
	[ListOfItems removeAllObjects];
    [ListOfItems1 removeAllObjects];

	//[AddBuddyidItems removeAllObjects];
    
	itemsearch.text =nil;
    
	[itemsearch resignFirstResponder];
    
	searching = NO;
    
	letUserSelectRow = YES;
    
	[tbl reloadData];
    
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar

{
    
	itemsearch.showsScopeBar = NO;
    
	[itemsearch setShowsCancelButton:NO animated:YES];
    
	return YES;
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar

{
    
	itemsearch.showsScopeBar = YES;
    
	[itemsearch setShowsCancelButton:YES animated:YES];
    
	itemsearch.autocorrectionType=UITextAutocorrectionTypeNo;
    
	return YES;
    
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText

{
    
	[ListOfItems removeAllObjects];
    [ListOfItems1 removeAllObjects];

	//[AddBuddyidItems removeAllObjects];
	
	k = 0;
    
	if([searchText length] > 0)
        
	{
        
		searching = YES;
        
		letUserSelectRow = YES;
        
		[self searchTableView];
        
	}
    
	else
        
	{
        
		searching = NO;
        
		letUserSelectRow = NO;
        
	}
    
	[tbl reloadData];
	
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar

{
	
	@try
	
	{
		
		tbl.scrollEnabled = YES;
		
		[tbl reloadData];
		
		[itemsearch resignFirstResponder];
		
	}
	
	@catch (NSException *e)
	
	{
		[itemsearch resignFirstResponder];
	}
    
}

- (void) searchTableView

{
    
	NSString *searchText = itemsearch.text;
    
	for (NSString *sTemp in table_list)
        
	{
        
		NSRange titleResultsRange = [sTemp rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
		if (titleResultsRange.length > 0)
            
		{
            
			[ListOfItems addObject:sTemp];
			
			//[AddBuddyidItems addObject:[buddyidarray objectAtIndex:k]];
			
		}
        
		k++;
	}
	for (NSString *sTemp1 in list)
        
	{
        
		NSRange titleResultsRange = [sTemp1 rangeOfString:searchText options:NSCaseInsensitiveSearch];
        
		if (titleResultsRange.length > 0)
            
		{
            
			[ListOfItems1 addObject:sTemp1];
			
			//[AddBuddyidItems addObject:[buddyidarray objectAtIndex:k]];
			
		}
        
		k++;
	}

    
    
	[tbl reloadData];
    
}

// ************************************ SearBar Function ******************************** //

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
