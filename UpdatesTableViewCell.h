//
//  UpdatesTableViewCell.h
//  UpdatesListView
//
//  Created by Tope on 10/11/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdatesTableViewCell : UITableViewCell
@property(nonatomic,retain )IBOutlet UIImageView *img;
@property(nonatomic,retain )IBOutlet UILabel *title;
@property(nonatomic,retain )IBOutlet UILabel *seller;
@property(nonatomic,retain )IBOutlet UILabel *startfrom;
@property(nonatomic,retain )IBOutlet UIButton *btn;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *av;
@end
