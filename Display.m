//
//  ViewController.m
//  NearestMe
//
//  Created by Apple on 28/09/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import "Display.h"
#import "UpdatesTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "Reachability.h"

@interface Display ()

@end

@implementation Display
@synthesize list;
@synthesize tbl;
@synthesize get_str;
@synthesize CLController;
@synthesize get_title;
@synthesize title_lbl;
@synthesize tbl_view;
@synthesize activity,activity1;
NSString *longt;
NSString *lant;
NSString *lontitute;
NSString *lantitute;


NSMutableArray *lat_array;
NSMutableArray *long_array;
NSMutableArray *icon_array;
NSMutableArray *name_array;
NSMutableArray *address_array;
NSString *city_lon;
NSString *city_lat;

- (void)viewDidLoad
{
    [super viewDidLoad];
    //////////////////////////
    
    [activity startAnimating];
    activity.hidden=NO;
    
    locationController = [[MyCLController alloc] init];
	locationController.delegate = self;
	[locationController.locationManager startUpdatingLocation];

    
   
  
}

- (void)locationUpdate:(CLLocation *)location
{
	//NSLog(@"%@",[location description]);
    
    
    
    
    NSString *locLat  = [NSString stringWithFormat:@"%lf", location.coordinate.latitude];
    NSString * locLong = [NSString stringWithFormat:@"%lf", location.coordinate.longitude];
    
    float lat_float=[locLat floatValue];
    float long_float=[locLong floatValue];
    
    
    [self call_json:locLat ln:locLong];
    
    [locationController.locationManager stopUpdatingLocation];
}

-(void)viewWillAppear:(BOOL)animated
{
   // [self call_json];
}



-(void)call_json:(NSString *)lat ln:(NSString *)longt
{
    
    lat_array=[[NSMutableArray alloc]init];
    long_array=[[NSMutableArray alloc]init];
    icon_array=[[NSMutableArray alloc]init];
    name_array=[[NSMutableArray alloc]init];
    address_array=[[NSMutableArray alloc]init];
    
    
    SBJSON *parser = [[SBJSON alloc] init];
    
    NSLog(@"OOLLO1:%@",logt);
    NSLog(@"OOLLO1:%@",lat);
    NSLog(@"get_str:%@",get_str);

   
    NSString *type_str=get_str;
        
    NSMutableArray *API_Array=[[NSMutableArray alloc]initWithObjects:@"AIzaSyAigmzYTEqbZYJOvG0rHfr5B0eue80-GYI",@"AIzaSyAigmzYTEqbZYJOvG0rHfr5B0eue80-GYI",@"AIzaSyBo1O729SbrnWx0WamXRvK3JVI2BQctzmw",@"AIzaSyA0DtuSndAGaOlB_AguQCEG8GQkcgU_dRQ",@"AIzaSyBQAWYEO74Y0stTG7GqikAiViTS_e305zM",@"AIzaSyDaMsVBF1tKigs9cOTpTKYHj2w7l9bCck8",@"AIzaSyDjjB9tO3Vy5JuQ329_jP8WvBgZVBbCcfw",@"AIzaSyAv87JhJiJo1LBGcBEKf3OQ1InMMMx3K5A",nil];
    
    NSString *key = [[NSString alloc] initWithFormat:@"%@", [API_Array objectAtIndex:random()%[API_Array count]]];
    
    
    NSLog(@"KEY:%@",key);
    
    lantitute=lat;
    lontitute=longt;
     
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%@,%@&radius=5000&types=%@&sensor=false&key=%@",lantitute,lontitute,type_str,key]]];
    NSLog(@"URL!:%@",request);
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    
    
    NSString *json_string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    
    
    
    NSDictionary *statuses = [parser objectWithString:json_string error:nil];
    
    
   // NSLog(@"GET:%@",statuses);
    
    NSArray *obj1=[statuses objectForKey:@"results"] ;
  
    for(NSDictionary *sub in obj1)
        
    {
        [icon_array addObject:[sub objectForKey:@"icon"] ];
        [name_array addObject:[sub objectForKey:@"name"] ];
        [address_array addObject:[sub objectForKey:@"vicinity"] ];
       [ lat_array addObject:[sub valueForKeyPath:@"geometry.location.lat"]];
         [ long_array addObject:[sub valueForKeyPath:@"geometry.location.lng"]];
      
        
    }
    
    NSUserDefaults *geo = [NSUserDefaults standardUserDefaults];
    [geo setObject:lat_array forKey:@"LATITUDE"];
     [geo setObject:long_array forKey:@"LONGITUDE"];
   
    
    
    NSLog(@"NAME:%@",name_array);
     NSLog(@"NAME:%@",icon_array);
     NSLog(@"NAME:%@",address_array);
    NSLog(@"LAT:%@",lat_array);
    NSLog(@"LONG:%@",long_array);
    [tbl reloadData];
    
    
    [activity stopAnimating];
    activity.hidden=YES;
   
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
   
    
    return [name_array count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
  UpdatesTableViewCell * cell = [tbl dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"UpdatesTableViewCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (UpdatesTableViewCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    CALayer *imageLayer = cell.img.layer;
    [imageLayer setCornerRadius:25];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    
    [cell.av startAnimating];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.title.text=[name_array objectAtIndex:indexPath.row];
    cell.startfrom.text=[address_array objectAtIndex:indexPath.row];
    
    NSString *temp=[icon_array objectAtIndex:indexPath.row];
    
    [cell.img setImageWithURL:[NSURL URLWithString:temp]
                 placeholderImage:nil];
    
    
    return cell;
}
-(IBAction)back:(id)sender
{
    [self dismissModalViewControllerAnimated:NO];
    
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
