//
//  ViewController.h
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

-(IBAction)News:(id)sender;
-(IBAction)History:(id)sender;
-(IBAction)Gallery:(id)sender;
-(IBAction)Report:(id)sender;
-(IBAction)More:(id)sender;
-(IBAction)Back:(id)sender;
@property(nonatomic,retain)IBOutlet UIImageView *adImg;
-(IBAction)AddClick:(id)sender;
@end
