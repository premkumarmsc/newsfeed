//
//  ViewController.h
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

-(IBAction)News:(id)sender;
-(IBAction)History:(id)sender;
-(IBAction)Gallery:(id)sender;
-(IBAction)Report:(id)sender;
-(IBAction)More:(id)sender;
-(IBAction)Back:(id)sender;
-(IBAction)Next:(id)sender;
-(IBAction)Previous:(id)sender;
@property(nonatomic,retain)IBOutlet UIView *galView;
@property(retain,nonatomic)IBOutlet UICollectionView *collectionViewIMAGE;
@property(nonatomic,retain)IBOutlet UIImageView *adImg;
-(IBAction)AddClick:(id)sender;
@end
