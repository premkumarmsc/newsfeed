//
//  ViewController.m
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import "ContactViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface ContactViewController ()

@end

@implementation ContactViewController
@synthesize imageButton;
@synthesize firstAsset;
UIActionSheet *actionSheetVideo;

int check_value;
int isCamera;




NSMutableArray *adImageArr;
NSMutableArray *adURLArr;
NSString *adurlString;
int addIS;
NSTimer *timer;

-(void)displayAds
{
    int length = [adImageArr count];
    // Get random value between 0 and 99
    int randomindex = arc4random() % length;
    
    
    @try {
         [_adImg setImageWithURL:[NSURL URLWithString:adImageArr[randomindex]]
               placeholderImage:[UIImage imageNamed:@"images.jpeg"]];
        
        adurlString=adURLArr[randomindex];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}


-(void)getAds
{
    
    addIS=1;
    
    adImageArr=[[NSMutableArray alloc]init];
    adURLArr=[[NSMutableArray alloc]init];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Adds"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
}

-(IBAction)emailClick
{
    NSLog(@"%@",_emailText.text);
    
    
   
    
    
    NSString *mobile=[NSString stringWithFormat:@"mailto://%@",_emailText.text];
    
    NSURL *URL = [NSURL URLWithString:mobile];
    [[UIApplication sharedApplication] openURL:URL];
}
-(IBAction)phoneClick
{
     NSLog(@"%@",_phoneText.text);
    
    NSString *mobile=[NSString stringWithFormat:@"tel://%@",_phoneText.text];
    
    NSURL *URL = [NSURL URLWithString:mobile];
    [[UIApplication sharedApplication] openURL:URL];
}

- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    
    if (addIS==0)
    {
        
        NSString *responseString = [request1 responseString];
        
        
        
        NSMutableData *results1 = [responseString JSONValue];
        
        NSLog(@"HELLO:%@",results1);
        
        
        NSArray *temp= [results1 valueForKeyPath:@"contact"];
        
      
        
        
        for(NSDictionary *value in temp)
        {
            
            NSString *ad=[value valueForKey:@"address"];
            NSString *ph=[value valueForKey:@"phoneno"];
            NSString *em=[value valueForKey:@"emailid"];
            
            
            NSLog(@"AD:%@",ad);
            
            
            _addressView.text=ad;
            _emailText.text=em;
            _phoneText.text=[NSString stringWithFormat:@"%@",ph];
        }
        
      
        
         [self getAds];
    }
    else
    {
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",results1);
    
    
    NSArray *temp= [results1 valueForKeyPath:@"adds"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
        [adImageArr  addObject:[value valueForKey:@"img_url"]];
        [adURLArr  addObject:[value valueForKey:@"addslink"]];
        
    }
    
    //[self displayAds];
    
    
    timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayAds) userInfo:nil repeats:YES];
    
    NSLog(@"TIT:%@",adImageArr);
    
    }
    
    
    
    
    
}


-(IBAction)AddClick:(id)sender
{
    NSLog(@"URL:%@",adurlString);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adurlString]];
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ENTER");
    
    [timer invalidate];
    timer=nil;
}












- (void)viewDidLoad
{
    [super viewDidLoad];
    check_value=0;
    isCamera=1;
    
    addIS=0;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Contact"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
   
    
	// Do any additional setup after loading the view, typically from a nib.
}
-(IBAction)Back:(id)sender
{
     [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)submit
{
    NSLog(@"Name:%@",_nameText.text);
     NSLog(@"Name:%@",_phoneText.text);
     NSLog(@"Name:%@",_emailText.text);
     NSLog(@"Name:%@",_descriptionText.text);
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_descriptionText resignFirstResponder];
     [_lastnameText resignFirstResponder];
    
    if ([_nameText.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"First Name is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        if ([_lastnameText.text isEqualToString:@""]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Last Name is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {

        if ([_phoneText.text isEqualToString:@""]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Phone Number is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            if ([_emailText.text isEqualToString:@""]) {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email Address is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
            else
            {
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =_emailText.text;
                
                
                if((_emailText.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Report Submitted Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alertView show];
                    
                    
                }
            }
  
        }
        }
        
    }
    
    
    
}
-(IBAction)cancel
{
    _nameText.text=@"";
    _phoneText.text=@"";
    _emailText.text=@"";
    _descriptionText.text=@"";
    _lastnameText.text=@"";
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_descriptionText resignFirstResponder];
      [_lastnameText resignFirstResponder];
}


- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)browseVideo
{
    
    check_value=1;
    
    NSString *actionSheetTitle = @"Select your Video"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"New Record";
    NSString *other2 = @"Directory";
    
    NSString *cancelTitle = @"Cancel";
   actionSheetVideo = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheetVideo showInView:self.view];
}
-(IBAction)browseImage
{
    
 
    check_value=0;
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet==actionSheetVideo)
    {
        if (buttonIndex==0)
        {
            isCamera=0;
            [self startCameraControllerFromViewController: self
                                            usingDelegate: self];
                       
        }
        else  if (buttonIndex==1)
        {
           
            
            isCamera=1;
            
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Saved Album Found"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
                [alert show];
            }else{
                
                [self startMediaBrowserFromViewController: self
                                            usingDelegate: self];
            }

        }
        
    }
    else
    {
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    }
    
}
- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    
    [controller presentModalViewController: cameraUI animated: YES];
    return YES;
}

- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentModalViewController: mediaUI animated: YES];
    return YES;
}



-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    if (check_value==0)
    {
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];;
       // [imageButton setImage:image forState:UIControlStateNormal];
        
        
        _camImg.image=image;
        
        
        
        
        [self dismissModalViewControllerAnimated:YES];//关闭模态视图控制器
    }
    else
    {
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        
        
        
        NSURL * movieURL = [info valueForKey:UIImagePickerControllerMediaURL] ;
        
        if (isCamera==0) {
            UISaveVideoAtPathToSavedPhotosAlbum([movieURL path], nil, nil, nil);
        }
        
        
        
        NSData * movieData = [NSData dataWithContentsOfURL:movieURL];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.MOV"];
        
        [movieData writeToFile:savedImagePath atomically:NO];
        
        
        
        [self dismissModalViewControllerAnimated:NO];
        // Handle a movie capture
        if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0)
            == kCFCompareEqualTo) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Awesome" message:@"Video Loaded successfully"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
            [alert show];
            
                       
            firstAsset = [AVAsset assetWithURL:[info objectForKey:UIImagePickerControllerMediaURL]];
            
            [self getThumpnail_image];
            
        }

    }
       
   
}
-(void)getThumpnail_image
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError * error;
    NSArray *directoryContents =  [[NSFileManager defaultManager]
                                   contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    
    for(NSString *strFile in directoryContents)
    {
        NSString *strVideoPath = [NSString stringWithFormat:@"%@/%@",documentsDirectory,strFile];
        UIImage *img = [self getThumbNail:strVideoPath];
        
        //[_videoButton setImage:img forState:UIControlStateNormal];
       
        _videoImg.image=img;

        
        //[arrVideoImages addObject:img];
        
    }
}
-(UIImage *)getThumbNail:(NSString*)stringPath
{
    NSURL *videoURL = [NSURL fileURLWithPath:stringPath];
    
    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    
    
    
    
    UIImage *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    //Player autoplays audio on init
    [player stop];
    
    return thumbnail;
}
- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    
    [self dismissModalViewControllerAnimated: YES];
}
- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissModalViewControllerAnimated: YES];
}


-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}





-(IBAction)News:(id)sender
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)History:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)Gallery:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)Report:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}


-(IBAction)More:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    
    
}







@end
