//
//  ViewController.m
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController


NSMutableArray *adImageArr;
NSMutableArray *adURLArr;
NSString *adurlString;
int addIS;
NSTimer *timer;

-(void)displayAds
{
    int length = [adImageArr count];
    // Get random value between 0 and 99
    int randomindex = arc4random() % length;
    
    
    @try {
        [_adImg setImageWithURL:[NSURL URLWithString:adImageArr[randomindex]]
               placeholderImage:[UIImage imageNamed:@"images.jpeg"]];
        
        adurlString=adURLArr[randomindex];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}


-(void)getAds
{
    
    addIS=1;
    
    adImageArr=[[NSMutableArray alloc]init];
    adURLArr=[[NSMutableArray alloc]init];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Adds"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",results1);
    
    
    NSArray *temp= [results1 valueForKeyPath:@"adds"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
        [adImageArr  addObject:[value valueForKey:@"img_url"]];
        [adURLArr  addObject:[value valueForKey:@"addslink"]];
        
    }
    
    //[self displayAds];
    
    
    timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayAds) userInfo:nil repeats:YES];
    
    NSLog(@"TIT:%@",adImageArr);
    
    
    
    
    
    
    
}



-(IBAction)AddClick:(id)sender
{
    NSLog(@"URL:%@",adurlString);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adurlString]];
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ENTER");
    
    [timer invalidate];
    timer=nil;
}










-(IBAction)Back:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getAds];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)News:(id)sender
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)History:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)Gallery:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)Report:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)More:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    
    
}
@end
