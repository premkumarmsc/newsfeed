//
//  ViewController.h
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ReportViewController : UIViewController<UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MPMediaPickerControllerDelegate>
{
    
}

@property(nonatomic,retain)IBOutlet UIImageView *camImg;
@property(nonatomic,retain)IBOutlet UIImageView *videoImg;

@property(nonatomic,retain)IBOutlet UITextField *nameText;
@property(nonatomic,retain)IBOutlet UITextField *lastnameText;
@property(nonatomic,retain)IBOutlet UITextField *phoneText;
@property(nonatomic,retain)IBOutlet UITextField *emailText;
@property(nonatomic,retain)IBOutlet UITextField *descriptionText;
@property(nonatomic,retain)IBOutlet UIButton *imageButton;
@property(nonatomic,retain)IBOutlet UIButton *videoButton;
@property(nonatomic,retain)IBOutlet UIButton *submitButton;
@property(nonatomic,retain)IBOutlet UIProgressView *progress;
@property(nonatomic,retain)AVAsset* firstAsset;
-(IBAction)News:(id)sender;
-(IBAction)History:(id)sender;
-(IBAction)Gallery:(id)sender;
-(IBAction)Report:(id)sender;
-(IBAction)More:(id)sender;

-(IBAction)submit;
-(IBAction)cancel;
-(IBAction)browseImage;
-(IBAction)browseVideo;
@property(nonatomic,retain)IBOutlet UIImageView *adImg;
-(IBAction)AddClick:(id)sender;
@end
