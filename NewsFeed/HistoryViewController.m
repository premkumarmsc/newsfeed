//
//  ViewController.m
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import "HistoryViewController.h"
#import "NewsCell.h"

@interface HistoryViewController ()

@end

@implementation HistoryViewController
NSMutableArray *newsIDArray;
NSMutableArray *newsTitle;
NSMutableArray *newsContent;
NSMutableArray *newsImage;
NSMutableArray *shortDes;
NSMutableArray *updatedDate;



NSMutableArray *adImageArr;
NSMutableArray *adURLArr;
NSString *adurlString;
int addIS;
NSTimer *timer;

-(void)displayAds
{
    int length = [adImageArr count];
    // Get random value between 0 and 99
    int randomindex = arc4random() % length;
    
    
    @try {
         [_adImg setImageWithURL:[NSURL URLWithString:adImageArr[randomindex]]
               placeholderImage:[UIImage imageNamed:@"images.jpeg"]];
        
        adurlString=adURLArr[randomindex];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}


-(void)getAds
{
    
    addIS=1;
    
    adImageArr=[[NSMutableArray alloc]init];
    adURLArr=[[NSMutableArray alloc]init];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Adds"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
}


-(IBAction)AddClick:(id)sender
{
    NSLog(@"URL:%@",adurlString);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adurlString]];
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ENTER");
    
    [timer invalidate];
    timer=nil;
}






- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    
    [self getCountry];
    
}
-(void)getCountry
{
    
    addIS=0;
    
    newsIDArray=[[NSMutableArray alloc]init];
    newsTitle=[[NSMutableArray alloc]init];
    newsContent=[[NSMutableArray alloc]init];
    newsImage=[[NSMutableArray alloc]init];
    shortDes=[[NSMutableArray alloc]init];
    updatedDate=[[NSMutableArray alloc]init];
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=History"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",results1);
    
    if (addIS==0)
    {
    
    NSArray *temp= [results1 valueForKeyPath:@"histroy"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
        [newsIDArray  addObject:[value valueForKey:@"id"]];
        [newsTitle  addObject:[value valueForKey:@"title"]];
        [shortDes  addObject:[value valueForKey:@"short_desc"]];
        [newsContent  addObject:[value valueForKey:@"full_detail"]];
        [updatedDate  addObject:[value valueForKey:@"updated_date"]];
        [newsImage  addObject:[value valueForKey:@"img_url"]];
    }
    
    NSLog(@"TIT:%@",newsTitle);
    [self getAds];
    [_newsTable reloadData];
    }
    
        else
        {
            NSArray *temp= [results1 valueForKeyPath:@"adds"];
            
            
            
            
            for(NSDictionary *value in temp)
            {
                [adImageArr  addObject:[value valueForKey:@"img_url"]];
                [adURLArr  addObject:[value valueForKey:@"addslink"]];
                
            }
            
            //[self displayAds];
            
            
            timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayAds) userInfo:nil repeats:YES];
            
            NSLog(@"TIT:%@",adImageArr);
        }
        

    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    
    return [newsIDArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    
    
    NewsCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"NewsCell" owner:nil options:nil];
        
        for (UIView *view in views)
        {
            if([view isKindOfClass:[UITableViewCell class]])
            {
                cell = (NewsCell*)view;
                //cell.img=@"date.png";
                
            }
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleGray;
    
    CALayer *imageLayer = cell.img.layer;
    [imageLayer setCornerRadius:25];
    [imageLayer setBorderWidth:0];
    [imageLayer setMasksToBounds:YES];
    
    [cell.img setImageWithURL:[NSURL URLWithString:newsImage[indexPath.row]]
             placeholderImage:nil];
    cell.title.text=newsTitle[indexPath.row];
    cell.category.text=shortDes[indexPath.row];
    
    return cell;
    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 61;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select %d",indexPath.row);
    
    //[_popoverController_bookmark dismissPopoverAnimated:YES];
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        NewsDetailViewController *newsView=[[NewsDetailViewController alloc]initWithNibName:@"NewsDetailViewController~iPhone4" bundle:nil];
        
        newsView.getView=@"HISTORY";
        newsView.getTitle=newsTitle[indexPath.row];
        newsView.getImage=newsImage[indexPath.row];
         newsView.getDate=updatedDate[indexPath.row];
        newsView.getDescription=newsContent[indexPath.row];
        
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        NewsDetailViewController *newsView=[[NewsDetailViewController alloc]initWithNibName:@"NewsDetailViewController" bundle:nil];
        
        newsView.getView=@"HISTORY";
        newsView.getTitle=newsTitle[indexPath.row];
        newsView.getImage=newsImage[indexPath.row];
        newsView.getDate=updatedDate[indexPath.row];
        newsView.getDescription=newsContent[indexPath.row];
        
        [self presentViewController:newsView animated:NO completion:nil];
    }

}





-(IBAction)News:(id)sender
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)History:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)Gallery:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)Report:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)More:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    
    
}@end
