//
//  ViewController.m
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import "GalleryViewController.h"
#import "GalleryCell.h"

@interface GalleryViewController ()

@end

@implementation GalleryViewController
@synthesize collectionViewIMAGE;
GalleryCell *cell;
NSMutableArray *newsImage;
NSTimer * myTimer;;



NSMutableArray *adImageArr;
NSMutableArray *adURLArr;
NSString *adurlString;
int addIS;
NSTimer *timer;

-(void)displayAds
{
    int length = [adImageArr count];
    // Get random value between 0 and 99
    int randomindex = arc4random() % length;
    
    
    @try {
         [_adImg setImageWithURL:[NSURL URLWithString:adImageArr[randomindex]]
               placeholderImage:[UIImage imageNamed:@"images.jpeg"]];
        
        adurlString=adURLArr[randomindex];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}


-(void)getAds
{
    
    addIS=1;
    
    adImageArr=[[NSMutableArray alloc]init];
    adURLArr=[[NSMutableArray alloc]init];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Adds"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
}


-(IBAction)AddClick:(id)sender
{
    NSLog(@"URL:%@",adurlString);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adurlString]];
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ENTER");
    
    [timer invalidate];
    timer=nil;
}






- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view addSubview:_galView];
    _galView.hidden=YES;
    
     [self getCountry];
     
 
    
   
   [self.collectionViewIMAGE registerNib:[UINib nibWithNibName:@"GalleryCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
    
    
   
    
}
-(void)getCountry
{
 
    newsImage=[[NSMutableArray alloc]init];
  
    addIS=0;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Gallery"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",results1);
    
    
    if (addIS==0) {
        
    
   
    
    
    NSArray *temp= [results1 valueForKeyPath:@"gallery"];
    
    
    
    
    for(NSDictionary *value in temp)
    {
       
        NSString *temp=[value valueForKey:@"img_url"];
        
        
        
        NSString *che1 = [temp stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"TEMP:%@",che1);

        
        [newsImage  addObject:che1];
    }
    
   // NSLog(@"TIT:%@",newsTitle);
    
    
    
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 568)
    {
        UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 150, 320, 250)];
        
        [scr setShowsHorizontalScrollIndicator:NO];
        [scr setShowsVerticalScrollIndicator:NO];
        
        scr.tag = 1;
        scr.autoresizingMask=UIViewAutoresizingNone;
        [_galView addSubview:scr];
        [self setupScrollView:scr];
        UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(25, 264, 480, 36)];
        
        [pgCtr setTag:[newsImage count]+2];
        pgCtr.numberOfPages=[newsImage count];
        pgCtr.autoresizingMask=UIViewAutoresizingNone;
        pgCtr.hidden=YES;
        [_galView addSubview:pgCtr];
    }
    else
    {
        
        UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 160, 320, 250)];
        
        [scr setShowsHorizontalScrollIndicator:NO];
        [scr setShowsVerticalScrollIndicator:NO];
        
        scr.tag = 1;
        scr.autoresizingMask=UIViewAutoresizingNone;
        [_galView addSubview:scr];
        [self setupScrollView:scr];
        UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(25, 264, 480, 36)];
        
        [pgCtr setTag:[newsImage count]+2];
        pgCtr.numberOfPages=[newsImage count];
        pgCtr.autoresizingMask=UIViewAutoresizingNone;
        pgCtr.hidden=YES;
        [_galView addSubview:pgCtr];
    }
    [self getAds];
    
    [collectionViewIMAGE reloadData];
    
   
    }
    else
    {
        NSArray *temp= [results1 valueForKeyPath:@"adds"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            
            
          
            
            
            
            [adImageArr  addObject:[value valueForKey:@"img_url"]];
            
            
            
            
            [adURLArr  addObject:[value valueForKey:@"addslink"]];
            
        }
        
        //[self displayAds];
        
        
        timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayAds) userInfo:nil repeats:YES];
        
        NSLog(@"TIT:%@",adImageArr);
    }

    
    
    
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return [newsImage count];
}


- (GalleryCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
 
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
        
              
       // cell.image_view.image=[UIImage imageNamed:[NSString stringWithFormat:@"%d.png",indexPath.row]];
        
       // cell.cell_title_Label.text=nameArr[indexPath.row];
    
    
    NSString *temp=[newsImage objectAtIndex:indexPath.row];
    
   
    
  
    
    [cell.image_view setImageWithURL:[NSURL URLWithString:temp]
                     placeholderImage:[UIImage imageNamed:@"vbigicon.png"]];
    
        return cell;
    
    
        
        
    
    
}

-(IBAction)Back:(id)sender
{
    _galView.hidden=YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:[newsImage count]+2];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    //int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    
    int nextPage = indexPath.row-1 ;
    
    if( nextPage!=[newsImage count] )
    {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else
    {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }

    
    _galView.hidden=NO;
}

- (void)setupScrollView:(UIScrollView*)scrMain
{
    
    
    for (int i=0; i<[newsImage count]; i++)
    {
        
       // UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"s%d.png",i]];
        
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width+25, 0, 265, 189)];
        
        imgV.contentMode=UIViewContentModeScaleToFill;
        
       // [imgV setImage:image];
        
        
        [imgV setImageWithURL:[NSURL URLWithString:newsImage[i]]
                 placeholderImage:nil];
        
        
        imgV.tag=i+1;
        
        [scrMain addSubview:imgV];
        
    }
    
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*[newsImage count], scrMain.frame.size.height)];
    
  //  myTimer=[NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:YES];
}

- (void)scrollingTimer {
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:[newsImage count]+2];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    if( nextPage!=[newsImage count] )
    {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else
    {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }
}


-(IBAction)Next:(id)sender
{
    
    
    
    
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:[newsImage count]+2];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    // if page is not 10, display it
    
    NSLog(@"NEXT:%d",nextPage);
    
    
    
    if( nextPage!=[newsImage count]-1 )
    {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else
    {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }

}
-(IBAction)Previous:(id)sender
{
    // access the scroll view with the tag
    UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
    // same way, access pagecontroll access
    UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:[newsImage count]+2];
    // get the current offset ( which page is being displayed )
    CGFloat contentOffset = scrMain.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) - 1 ;
    // if page is not 10, display it
    if( nextPage!=[newsImage count] )
    {
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=nextPage;
        // else start sliding form 1 :)
    } else
    {
        [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
        pgCtr.currentPage=0;
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)News:(id)sender
{
    
    [myTimer invalidate];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)History:(id)sender
{
     [myTimer invalidate];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)Gallery:(id)sender
{
     [myTimer invalidate];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)Report:(id)sender
{
     [myTimer invalidate];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)More:(id)sender
{
     [myTimer invalidate];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    
    
}
@end
