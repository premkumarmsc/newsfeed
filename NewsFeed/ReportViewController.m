//
//  ViewController.m
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import "ReportViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Base64.h"
@interface ReportViewController ()

@end

@implementation ReportViewController
@synthesize imageButton;
@synthesize firstAsset;
UIActionSheet *actionSheetVideo;

int check_value;
int isCamera;


NSMutableArray *adImageArr;
NSMutableArray *adURLArr;
NSString *adurlString;
int addIS;
NSTimer *timer;

-(void)displayAds
{
    int length = [adImageArr count];
    // Get random value between 0 and 99
    int randomindex = arc4random() % length;
    
    
    @try {
         [_adImg setImageWithURL:[NSURL URLWithString:adImageArr[randomindex]]
               placeholderImage:[UIImage imageNamed:@"images.jpeg"]];
        
        adurlString=adURLArr[randomindex];
    }
    @catch (NSException *exception) {
        
    }
    
    
    
}
- (void)requestFinished:(ASIHTTPRequest *)request1
{
    
    NSString *responseString = [request1 responseString];
    
    
    
    NSMutableData *results1 = [responseString JSONValue];
    
    NSLog(@"HELLO:%@",results1);
    
    
           NSArray *temp= [results1 valueForKeyPath:@"adds"];
        
        
        
        
        for(NSDictionary *value in temp)
        {
            [adImageArr  addObject:[value valueForKey:@"img_url"]];
            [adURLArr  addObject:[value valueForKey:@"addslink"]];
            
        }
        
        //[self displayAds];
        
        
        timer=[NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(displayAds) userInfo:nil repeats:YES];
    
        NSLog(@"TIT:%@",adImageArr);
    
    
    
    
    
    
    
}

-(void)getAds
{
    
    addIS=1;
    
    adImageArr=[[NSMutableArray alloc]init];
    adURLArr=[[NSMutableArray alloc]init];
    
    
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/setout.php?Token=Adds"]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDelegate:self];
    [request startAsynchronous];
    
}


-(IBAction)AddClick:(id)sender
{
    NSLog(@"URL:%@",adurlString);
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adurlString]];
}


-(void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"ENTER");
    
    [timer invalidate];
    timer=nil;
}





- (void)viewDidLoad
{
    [super viewDidLoad];
    check_value=0;
    isCamera=1;
    
    
     _progress.hidden=YES;
    _submitButton.enabled=YES;
    
    _progress.frame=CGRectMake(20, 30, 263, 9);
    
    
    NSUserDefaults *gh=[NSUserDefaults standardUserDefaults];
    
    [gh setObject:@"" forKey:@"VIDEO_URL"];
    
    
    [self getAds];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)submit
{
    NSLog(@"Name:%@",_nameText.text);
     NSLog(@"Name:%@",_phoneText.text);
     NSLog(@"Name:%@",_emailText.text);
     NSLog(@"Name:%@",_descriptionText.text);
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_descriptionText resignFirstResponder];
     [_lastnameText resignFirstResponder];
    
    if ([_nameText.text isEqualToString:@""]) {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"First Name is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        if ([_lastnameText.text isEqualToString:@""]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Last Name is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {

        if ([_phoneText.text isEqualToString:@""]) {
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Phone Number is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else
        {
            if ([_emailText.text isEqualToString:@""]) {
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email Address is Mandotory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
            }
            else
            {
                NSString* emailRegex = @"[A-Z0-9a-z._%+]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
                NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
                NSString *subjectString =_emailText.text;
                
                
                if((_emailText.text.length!=0) && ([emailTest evaluateWithObject:subjectString] != YES))
                {
                    
                    if ([emailTest evaluateWithObject:subjectString] != YES)
                        
                    {
                        
                        
                        UIAlertView *loginalert = [[UIAlertView alloc] initWithTitle:@"Enter Valid Email ID" message:@"abc@example.com format" delegate:self
                                                                   cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        
                        [loginalert show];
                        
                        
                    }
                    
                    
                }
                
                else
                {
                    
                    
                    [self addcontectFun];
                    
                    
                }
            }
  
        }
        }
        
    }
    
    
    
}


-(void)addcontectFun
{
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.mobileappslebanon.com/site/mobile/admin/php/add_report.php"]];
    
    __block  ASIFormDataRequest *request_post1234 = [ASIFormDataRequest requestWithURL:url];
    
    
    
    NSUserDefaults *check=[NSUserDefaults standardUserDefaults];
    NSString *userImage=[check objectForKey:@"USER_IMAGE"];
    NSString *userVideo=[check objectForKey:@"VIDEO_URL"];
    
  
       
    
    if ([userImage isEqualToString:@"NULL"])
    {
        userImage=@"http://www.socialnetgateapp.com/bundles/acmeregistration/images/no-user-image.png";
    }
    
    
    //////(@"USER IMAGE:%@",userImage);
    
    
  
    
    
    
    
    [request_post1234 setPostValue:@"true" forKey:@"report"];
    [request_post1234 setPostValue:_nameText.text forKey:@"first_name"];
    [request_post1234 setPostValue:_lastnameText.text forKey:@"last_name"];
    [request_post1234 setPostValue:_emailText.text forKey:@"email"];
    [request_post1234 setPostValue:_descriptionText.text forKey:@"address"];
    [request_post1234 setPostValue:_phoneText.text forKey:@"phone"];
     [request_post1234 setPostValue:userImage forKey:@"img_url"];
    [request_post1234 setPostValue:userVideo forKey:@"video_url"];
    
    
    
    [request_post1234 setTimeOutSeconds:30];
    
    
    [request_post1234 setCompletionBlock:^{
        NSString *responseString23 = [request_post1234 responseString];
        NSMutableData *results11 = [responseString23 JSONValue];
        
        
        NSLog(@"RESULTS LOGIN:%@",responseString23);
        
        
        NSString *des=[results11 valueForKeyPath:@"header.description"];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Information" message:des delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
        
        
        [self viewDidLoad];
        
    }];
    [request_post1234 setFailedBlock:^{
        NSError *error = [request_post1234 error];
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@" "
                                                            message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:self
                                                  cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        
        
        
    }];
    
    [request_post1234 startAsynchronous];
    
}




-(IBAction)cancel
{
    _nameText.text=@"";
    _phoneText.text=@"";
    _emailText.text=@"";
    _descriptionText.text=@"";
    _lastnameText.text=@"";
    [_nameText resignFirstResponder];
    [_phoneText resignFirstResponder];
    [_emailText resignFirstResponder];
    [_descriptionText resignFirstResponder];
      [_lastnameText resignFirstResponder];
}


- (BOOL)textFieldShouldReturn: (UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)browseVideo
{
    
    check_value=1;
    
    NSString *actionSheetTitle = @"Select your Video"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"New Record";
    NSString *other2 = @"Directory";
    
    NSString *cancelTitle = @"Cancel";
   actionSheetVideo = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheetVideo showInView:self.view];
}
-(IBAction)browseImage
{
    
 
    check_value=0;
    
    NSString *actionSheetTitle = @"Select your Picture"; //Action Sheet Title
    // NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Camera";
    NSString *other2 = @"Album";
    
    NSString *cancelTitle = @"Cancel";
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:other1, other2,  nil];
    [actionSheet showInView:self.view];
    
    
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet==actionSheetVideo)
    {
        if (buttonIndex==0)
        {
            isCamera=0;
            [self startCameraControllerFromViewController: self
                                            usingDelegate: self];
                       
        }
        else  if (buttonIndex==1)
        {
           
            
            isCamera=1;
            
            if ([UIImagePickerController isSourceTypeAvailable:
                 UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"No Saved Album Found"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
                [alert show];
            }else{
                
                [self startMediaBrowserFromViewController: self
                                            usingDelegate: self];
            }

        }
        
    }
    else
    {
    if (buttonIndex==0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = NO;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"The device does not have camera" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            // [alert release];
        }
        
    }
    else  if (buttonIndex==1)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
    }
    
}
- (BOOL) startCameraControllerFromViewController: (UIViewController*) controller
                                   usingDelegate: (id <UIImagePickerControllerDelegate,
                                                   UINavigationControllerDelegate>) delegate {
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeCamera] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    // Displays a control that allows the user to choose movie capture
    cameraUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;
    
    cameraUI.delegate = delegate;
    
    [controller presentModalViewController: cameraUI animated: YES];
    return YES;
}

- (BOOL) startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    
    mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = delegate;
    
    [controller presentModalViewController: mediaUI animated: YES];
    return YES;
}



-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    if (check_value==0)
    {
        UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];;
       // [imageButton setImage:image forState:UIControlStateNormal];
        
        
        _camImg.image=image;
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postProImgfile",@"http://api.socialnetgateapp.com/v1"]];
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        //[request_post setFile:@"/Users/apple/Desktop/Free-Grammar-Ebook-Level-2.pdf" forKey:@"filename"];
        
        //UIImage * ourImage = [UIImage imageNamed:@"rajini-bday2.jpg"];
        NSData * ourImageData = UIImageJPEGRepresentation(image, 100);
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
        [dateFormat setDateFormat:@"HH:mm:ss zzz"];
        NSString *dateString = [dateFormat stringFromDate:date];
        
        //////(@"DATE STRING:%@",dateString);
        
        NSString *newString = [dateString stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@":" withString:@""];
        NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"GMT" withString:@""];
        NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@"+" withString:@""];
        
        NSString *imageName=[NSString stringWithFormat:@"%@.jpg",newString3];
        
        
        
        [request_post addData:ourImageData withFileName:imageName andContentType:@"image/jpeg" forKey:@"file"];
        _progress.hidden=NO;
         _submitButton.enabled=NO;
        [request_post setTimeOutSeconds:60];
          [request_post setUploadProgressDelegate:_progress];
        
        [request_post setCompletionBlock:^{
            // Use when fetching text data
            NSString *responseString = [request_post responseString];
            
            
            //////(@"RES:%@",responseString);
            
            NSMutableData *results11 = [responseString JSONValue];
            
            
            NSLog(@"RESULTS LOGIN:%@",results11);
            
            
            NSString *temp= [results11 valueForKeyPath:@"photos.profile_img_url"];
            
            
            NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
            
            [imgtemp setObject:temp forKey:@"USER_IMAGE"];
            
            _progress.hidden=YES;
             _submitButton.enabled=YES;
            
            
            if ([temp length]==0) {
                
                NSString *temp= [results11 valueForKeyPath:@"error.text"];
                
                
                NSUserDefaults *imgtemp=[NSUserDefaults standardUserDefaults];
                
                [imgtemp setObject:@"NULL" forKey:@"USER_IMAGE"];
                
                
                
                
            }
            
            // Use when fetching binary data
            // NSData *responseData = [request_post responseData];
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            
            
            
            
        }];
        [request_post startAsynchronous];
        
        
      
        
        
        [self dismissModalViewControllerAnimated:YES];//关闭模态视图控制器
    }
    else
    {
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        
        
        
        NSURL * movieURL = [info valueForKey:UIImagePickerControllerMediaURL] ;
        
        if (isCamera==0) {
            UISaveVideoAtPathToSavedPhotosAlbum([movieURL path], nil, nil, nil);
        }
        
        
        
        NSData * movieData = [NSData dataWithContentsOfURL:movieURL];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *getVideoPathNew = [documentsDirectory stringByAppendingPathComponent:@"savedImage.MOV"];
        
        [movieData writeToFile:getVideoPathNew atomically:NO];
        
        
       
        
       
        

        
        
       
        
        
        
        
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/postfile",@"http://api.socialnetgateapp.com/v1"]];
        
        
        __block  ASIFormDataRequest *request_post = [ASIFormDataRequest requestWithURL:url];
        
        
        _progress.hidden=NO;
         _submitButton.enabled=NO;
        
        [request_post setFile:getVideoPathNew forKey:@"uploadedfile"];
        
        
        // NSString *str = @"E,PH,RO,N1,3S,Y,S5,4,321";//
        
        NSString *str1 = @"EPHRON13SYS54321"; // 16 Digit Code
        
        NSMutableString *mu = [NSMutableString stringWithString:str1];
        [mu insertString:@"," atIndex:1];
        [mu insertString:@"," atIndex:4];
        [mu insertString:@"," atIndex:7];
        [mu insertString:@"," atIndex:10];
        [mu insertString:@"," atIndex:13];
        [mu insertString:@"," atIndex:15];
        [mu insertString:@"," atIndex:18];
        [mu insertString:@"," atIndex:20];
        
        //  ////(@"MUT:%@",mu);
        
        
        NSArray  *testArray2 = [mu componentsSeparatedByString:@","];
        NSArray  *algorithm_arr=[NSArray arrayWithObjects:@"7",@"1",@"3",@"5",@"4",@"1",@"6",@"9",@"4", nil];
        
        // ////(@"TEST Array 2:%@",testArray2);
        
        
        NSMutableString* message = [NSMutableString stringWithCapacity:100];
        
        
        // ////(@"MESSAGEGHGH:%@",message);
        for (int j=0; j<[algorithm_arr count]; j++)
        {
            NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            NSMutableString *randomString = [NSMutableString stringWithCapacity: [algorithm_arr[j]intValue]];
            
            for (int i=0; i<[algorithm_arr[j]intValue]; i++)
            {
                [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
            }
            
            // ////(@"RANDOM:%@",randomString);
            
            
            NSString *arr_str=testArray2[j];
            
            [message appendString:[NSString stringWithFormat:@"%@%@",testArray2[j],randomString]];
            
            // [message deleteCharactersInRange:NSMakeRange([message length] - 1, 1)];
            
            
            
        }
        
        
        // ////(@"MESSAGE:%@",message);
        
        
        
        NSData *plainTextData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSString *base64String = [plainTextData base64EncodedString];
        
        
        [request_post setPostValue:base64String forKey:@"apikey"];
        
        
        [request_post setShowAccurateProgress: YES];
        [request_post setUploadProgressDelegate:_progress];
        // UPLOAD APP ENTER BACKGROUND //
        [request_post setShouldContinueWhenAppEntersBackground:YES];
        
        
        //[request_post setUploadProgressDelegate:progressBar];
        
        
        
        
        
        [request_post setCompletionBlock:^{
            // Use when fetching text data
            NSString *responseString = [request_post responseString];
            
            
            
            
            ////(@"RES:%@",responseString);
            
            NSMutableData *results11 = [responseString JSONValue];
            
            
            //////(@"RESULTS LOGIN:%@",results11);
            
            
            NSString *temp= [results11 valueForKeyPath:@"photo_url"];
            
            
            ////(@"AUDIO URL:%@",temp);

            NSUserDefaults *gh=[NSUserDefaults standardUserDefaults];
            
            [gh setObject:temp forKey:@"VIDEO_URL"];
            
            
            
            NSLog(@"RESULTS LOGIN:%@",temp);
            
            _progress.hidden=YES;
             _submitButton.enabled=YES;
                       // Use when fetching binary data
            // NSData *responseData = [request_post responseData];
        }];
        [request_post setFailedBlock:^{
            NSError *error = [request_post error];
            
            _progress.hidden=YES;
            _submitButton.enabled=YES;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed!" message:@"Video Upload Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
             [alert show];
            
            
            
        }];
        [request_post startAsynchronous];
        
        
       
        
        
        [self dismissModalViewControllerAnimated:YES];//关闭模态视图控制器
       
        // Handle a movie capture
        if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0)
            == kCFCompareEqualTo) {
            
            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Awesome" message:@"Video Loaded successfully"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
           // [alert show];
            
                       
            firstAsset = [AVAsset assetWithURL:[info objectForKey:UIImagePickerControllerMediaURL]];
            
            [self getThumpnail_image];
            
        }

    }
       
   
}
-(void)getThumpnail_image
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSError * error;
    NSArray *directoryContents =  [[NSFileManager defaultManager]
                                   contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    
    for(NSString *strFile in directoryContents)
    {
        NSString *strVideoPath = [NSString stringWithFormat:@"%@/%@",documentsDirectory,strFile];
        UIImage *img = [self getThumbNail:strVideoPath];
        
        //[_videoButton setImage:img forState:UIControlStateNormal];
       
        _videoImg.image=img;

        
        //[arrVideoImages addObject:img];
        
    }
}
-(UIImage *)getThumbNail:(NSString*)stringPath
{
    NSURL *videoURL = [NSURL fileURLWithPath:stringPath];
    
    MPMoviePlayerController *player = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    
    
    
    
    UIImage *thumbnail = [player thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    
    //Player autoplays audio on init
    [player stop];
    
    return thumbnail;
}
- (void) mediaPicker: (MPMediaPickerController *) mediaPicker didPickMediaItems: (MPMediaItemCollection *) mediaItemCollection
{
    
    [self dismissModalViewControllerAnimated: YES];
}
- (void) mediaPickerDidCancel: (MPMediaPickerController *) mediaPicker
{
    [self dismissModalViewControllerAnimated: YES];
}


-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissModalViewControllerAnimated:YES];
}





-(IBAction)News:(id)sender
{
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ViewController *newsView=[[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)History:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        HistoryViewController *newsView=[[HistoryViewController alloc]initWithNibName:@"HistoryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}
-(IBAction)Gallery:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        GalleryViewController *newsView=[[GalleryViewController alloc]initWithNibName:@"GalleryViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
}
-(IBAction)Report:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    if(result.height == 568)
    {
        ReportViewController *newsView=[[ReportViewController alloc]initWithNibName:@"ReportViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
    }
    
    
    
}


-(IBAction)More:(id)sender
{
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController~iPhone4" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    if(result.height == 568)
    {
        MoreViewController *newsView=[[MoreViewController alloc]initWithNibName:@"MoreViewController" bundle:nil];
        [self presentViewController:newsView animated:NO completion:nil];
        
    }
    
    
}







@end
