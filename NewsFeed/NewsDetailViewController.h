//
//  ViewController.h
//  NewsFeed
//
//  Created by ephronsystems on 9/26/13.
//  Copyright (c) 2013 PhononInfotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController
@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,retain)IBOutlet UILabel *titleLabel;
-(IBAction)News:(id)sender;
-(IBAction)History:(id)sender;
-(IBAction)Gallery:(id)sender;
-(IBAction)Report:(id)sender;
-(IBAction)More:(id)sender;
-(IBAction)Back:(id)sender;
@property(nonatomic,retain)NSString *getView;
@property(nonatomic,retain)NSString *getImage;
@property(nonatomic,retain)NSString *getTitle;
@property(nonatomic,retain)NSString *getDescription;
@property(nonatomic,retain)NSString *getDate;
@property(nonatomic,retain)IBOutlet UIImageView *adImg;
-(IBAction)AddClick:(id)sender;
@end
