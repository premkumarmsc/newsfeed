//
//  ViewController.h
//  NearestMe
//
//  Created by Apple on 28/09/12.
//  Copyright (c) 2012 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "CoreLocationController.h"
#import <CoreLocation/CoreLocation.h>
#import "MyCLController.h"
double *lon1;
double *lan1;
NSString *logt;
NSString *lat;
@interface Display : UIViewController<MKMapViewDelegate,CoreLocationControllerDelegate,MyCLControllerDelegate>
{
    CLLocationCoordinate2D theCoordinate;
	CLLocationManager *currentLocation;
    
     CLLocationManager *locationManager;
    IBOutlet UILabel *speedLabel;
	IBOutlet UILabel *latitudeLabel;
	IBOutlet UILabel *longitudeLabel;
	IBOutlet UILabel *altitudeLabel;
	NSString*speed1;
	NSString*speed2;
     MyCLController *locationController;
}
@property(nonatomic,retain)NSMutableArray *list;
@property(nonatomic,retain)IBOutlet UITableView *tbl;
@property(nonatomic,retain)NSString *get_str;

@property(nonatomic,retain)NSString *get_title;

@property (nonatomic, retain) CoreLocationController *CLController;
@property (nonatomic, retain)IBOutlet UILabel *title_lbl;

@property(nonatomic,retain)IBOutlet UIView *tbl_view;

@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activity;
@property(nonatomic,retain)IBOutlet UIActivityIndicatorView *activity1;
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
-(IBAction)update:(id)sender;
-(IBAction)back:(id)sender;
-(IBAction)map:(id)sender;
@end
